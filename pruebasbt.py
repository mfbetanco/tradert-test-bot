# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

# Import the backtrader platform
import backtrader as bt
import pprint, math

RSI_PERIOD = 14
RSI_OVERBOUGHT = 70
RSI_OVERSOLD = 30
TRADE_SYMBOL = 'BNBUSDT'

# Create a Stratey
class TestStrategy(bt.Strategy):

    def log(self, txt, dt=None):
        ''' Logging function fot this strategy'''
        dt = dt or self.datas[0].datetime.date(0)
        print('%s, %s' % (dt.isoformat(), txt))

    def __init__(self):
        # Keep a reference to the "close" line in the data[0] dataseries
        self.dataclose = self.datas[0].close

        # To keep track of pending orders and buy price/commission
        self.order = None
        self.buyprice = None
        self.buycomm = None
        self.order_size = None
        
        self.rsi = bt.talib.RSI(self.data, period=14)
        
        self.numorders = 0

    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enough cash
        if order.status in [order.Completed]:
            if order.isbuy():
                # self.log(
                #     'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                #     (order.executed.price,
                #      order.executed.value,
                #      order.executed.comm))

                self.buyprice = order.executed.price
                self.buycomm = order.executed.comm
            # else:  # Sell
                # self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                #          (order.executed.price,
                #           order.executed.value,
                #           order.executed.comm))

            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        self.order = None

    def notify_trade(self, trade):
        if trade.isclosed:
            self.log('TRADE PROFIT, GROSS %.2f, NET %.2f' %
                     (trade.pnl, trade.pnlcomm))

        elif trade.justopened:
            self.log('TRADE OPENED, SIZE %2d' % trade.size)

    def next(self):
        # Simply log the closing price of the series from the reference
        # self.log('Close, %.2f' % self.dataclose[0])

        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return

        # Check if we are in the market
        if not self.position:

            # Not yet ... we MIGHT BUY if ...
            if self.rsi[0] < RSI_OVERSOLD and self.rsi[-1] < RSI_OVERSOLD and self.rsi[-2] < RSI_OVERSOLD:
                # BUY, BUY, BUY!!! (with default parameters)
                # self.log('BUY CREATE, %.2f' % self.dataclose[0])
                # self.log('RSI, %.2f' % self.rsi[0])

                # Keep track of the created order to avoid a 2nd order
                self.order_size = math.trunc((3000 / self.dataclose[0]) * 100) / 100
                # print(self.order_size)
                self.order = self.buy(size=self.order_size)

        else:

            # Already in the market ... we might sell
            # if len(self) >= (self.bar_executed + 5):
            sell_price = self.buyprice * 1.05
            # if (self.buyprice * 1.05) < self.dataclose[0]:
                # SELL, SELL, SELL!!! (with all possible default parameters)
            # self.log('SELL CREATE, %.2f' % self.dataclose[0])

            # Keep track of the created order to avoid a 2nd order
            self.order = self.sell(exectype=bt.Order.Limit, price=sell_price, 
                                       size=self.order_size)

if __name__ == '__main__':
    cerebro = bt.Cerebro()

    # Add a strategy
    cerebro.addstrategy(TestStrategy)

    data = bt.feeds.GenericCSVData(dataname='15minute_2021_ETHUSDT.csv', 
                                   dtformat=2)
    cerebro.adddata(data)
    cerebro.broker.setcash(8000.0)

    # 0.1% ... divide by 100 to remove the %
    cerebro.broker.setcommission(commission=0.0075)

    print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())

    cerebro.run()

    print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())

    cerebro.plot()